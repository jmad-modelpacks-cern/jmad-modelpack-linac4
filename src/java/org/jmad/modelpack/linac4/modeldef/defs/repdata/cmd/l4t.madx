/*****************************************************
 *
 * MADX file for the L4T line from LINAC4 to LINAC2
 *
 * Directory: /afs/cern.ch/eng/ps/cps/TransLines/Linac4/2015/
 *
 * Execute with:  >madx < l4t.madx
 * This file is for H- at Ekin=160MeV
 *****************************************************/


/*****************************************************************************
 * TITLE
 *****************************************************************************/
 title, 'L4T 2015 optics. H- E_kin=160 MeV';

 option, echo;
!option, RBARC=FALSE;


/*****************************************************************************
 * L4TB
 * NB! The order of the .ele .str and .seq files matter.
 *     The reason is a >feature< of MADX
 *
 *****************************************************************************/

/* L4T.MBH.0250    : SBEND       , L := 0.8760411707588931;      ! L=0.870*0.4072434484/(2*sin(0.4072434484/2)) = 0.8760411707588931  EDMS 1269423 Pierre Thonet  magnetic length = 870 mm
 L4T.MBH.0450    : SBEND       , L := 0.8760411707588931;      ! L=0.870*0.4072434484/(2*sin(0.4072434484/2)) = 0.8760411707588931  EDMS 1269423 Pierre Thonet  magnetic length = 870 mm
 L4T.MBH.0650    : SBEND       , L := 0.8760411707588931;      ! L=0.870*0.4072434484/(2*sin(0.4072434484/2)) = 0.8760411707588931  EDMS 1269423 Pierre Thonet  magnetic length = 870 mm
 L4T.MBV.1250    : SBEND       , L := 0.8723174531799386;      ! L=0.870*0.2526079601/(2*sin(0.2526079601/2)) = 0.8723174531799386  EDMS 1269423 Pierre Thonet  magnetic length = 870 mm
 L4T.MBV.1550    : SBEND       , L := 0.8723174531799386;      ! L=0.870*0.2526079601/(2*sin(0.2526079601/2)) = 0.8723174531799386  EDMS 1269423 Pierre Thonet  magnetic length = 870 mm 
*/

 

 call, file = '../elements/l4t.ele';
 call, file = 'l4t.seq';
/* SEQEDIT, SEQUENCE=L4T;
      REMOVE, element=  L4T.MBH.0250IN  ;
      REMOVE, element=  L4T.MBH.0250OUT ;
      REMOVE, element=  L4T.MBH.0450IN  ;
      REMOVE, element=  L4T.MBH.0450OUT ;
      REMOVE, element=  L4T.MBH.0650IN  ;
      REMOVE, element=  L4T.MBH.0650OUT ;
      REMOVE, element=  L4T.MBV.1250IN  ;
      REMOVE, element=  L4T.MBV.1250OUT ;
      REMOVE, element=  L4T.MBV.1550IN  ;
      REMOVE, element=  L4T.MBV.1550OUT ;
 ENDEDIT;*/
 call, file = '../strength/l4t.str';
 call, file = '../aperture/l4t.dbx';


/*****************************************************************************
 * set initial twiss parameters
 *****************************************************************************/
call, file = '../inp/l4t.inp';


/*****************************************************************************
 * store initial parameters in memory block
 *****************************************************************************/
 INITBETA0: BETA0,
  BETX=BETX0,
  ALFX=ALFX0,
  MUX=MUX0,
  BETY=BETY0,
  ALFY=ALFY0,
  MUY=MUY0,
  X=X0,
  PX=PX0,
  Y=Y0,
  PY=PY0,
  T=T0,
  PT=PT0,
  DX=DX0,
  DPX=DPX0,
  DY=DY0,
  DPY=DPY0;


/*******************************************************************************
 * Beam
 * NB! beam->ex == (beam->exn)/(beam->gamma*beam->beta*4)
 *******************************************************************************/
 BEAM, PARTICLE=Hminus, MASS=.93929, CHARGE=-1, ENERGY=1.09929;
!show, beam;


/*******************************************************************************
 * survey
 *******************************************************************************/
/* SEQEDIT, SEQUENCE=L4T;
      REMOVE, element=  L4T.MBH.0250IN  ;
      REMOVE, element=  L4T.MBH.0250OUT ;
      REMOVE, element=  L4T.MBH.0450IN  ;
      REMOVE, element=  L4T.MBH.0450OUT ;
      REMOVE, element=  L4T.MBH.0650IN  ;
      REMOVE, element=  L4T.MBH.0650OUT ;
      REMOVE, element=  L4T.MBV.1250IN  ;
      REMOVE, element=  L4T.MBV.1250OUT ;
      REMOVE, element=  L4T.MBV.1550IN  ;
      REMOVE, element=  L4T.MBV.1550OUT ;
 ENDEDIT;*/

 title, "L4T INPUT FOR GEODE";

   sur2 : macro={

        call, file = 'l4t.seq';

        /*SEQEDIT, SEQUENCE=L4T;
             REMOVE, element=  L4T.MBH.0250IN  ;
             REMOVE, element=  L4T.MBH.0250OUT ;
             REMOVE, element=  L4T.MBH.0450IN  ;
             REMOVE, element=  L4T.MBH.0450OUT ;
             REMOVE, element=  L4T.MBH.0650IN  ;
             REMOVE, element=  L4T.MBH.0650OUT ;
             REMOVE, element=  L4T.MBV.1250IN  ;
             REMOVE, element=  L4T.MBV.1250OUT ;
             REMOVE, element=  L4T.MBV.1550IN  ;
             REMOVE, element=  L4T.MBV.1550OUT ;
        ENDEDIT;*/
        call, file = '../strength/l4t.str';

        use, sequence=L4T;

        X0     =    -1905.00325  ;
        Y0     =     2431.16062  ;
        Z0     =     1946.16673  ;
        THETA0 =    -5.4115944   ;
        set,  format="-17s";
        set,  format="17.12f";
        use sequence=L4T;
        select, flag=survey,clear;
        select, flag=survey, column=NAME,S,L,ANGLE,X,Y,Z,THETA,PHI,PSI,GLOBALTILT,SLOT_ID;
        survey, x0= X0, y0= Y0, z0= Z0,
                theta0= THETA0, phi0=  0.0000000000, psi0= 0.0000000000,
                file="../out/l4t_input_for_GEODE.sur";

        XX0    =     1905.00325  ;
        YY0    =     1946.16673  ;
        ZZ0    =     2431.16062  ;
        THETA0 =   2.4423872340  ;
        set,  format="-17s";vary, name=ds450 ,step = 0.00000001;
        set,  format="17.12f";
        use, sequence=L4T;
        select, flag=survey,clear;
        select, flag=survey, column=NAME,S,L,ANGLE,Z,X,Y,THETA,PHI,PSI,GLOBALTILT,SLOT_ID;
        survey, x0= YY0, y0= ZZ0, z0= XX0,
                theta0= THETA0, phi0=  0.0000000000, psi0= 0.0000000000,
                file="../out/l4t.sur";
   }
   exec, sur2; ! NB: The result of the survey is in the file:"l4t_input_for_GEODE.sur"



/*******************************************************************************
 * Rematching the magnetic length of MBH250
 *******************************************************************************/
 ! The position of MBH250 was modified by ds250
 
 /* match,use_macro;
        vary, name=ds250 ,step = 0.00000001;
        vary, name=ds450 ,step = 0.00000001;
        vary, name=ds650 ,step = 0.00000001;
        vary, name=ds1250 ,step = 0.00000001;
        vary, name=ds1550 ,step = 0.00000001; 
       
        use_macro,name=sur2;
        
        constraint, expr=table(survey, L4T.BCT.1553  ,X)= 2001.648062493645 ; !from SURVEY (MADX convention) file
        constraint, expr=table(survey, L4T.BCT.1553  ,Y)= 2433.659919999981 ;
        constraint, expr=table(survey, L4T.BCT.1553  ,Z)= 1912.882152074887 ;

        constraint, expr=table(survey, LT.BHZ20      ,X)= 2010.806303789647 ;
        constraint, expr=table(survey, LT.BHZ20      ,Y)= 2433.659919999981 ;
        constraint, expr=table(survey, LT.BHZ20      ,Z)= 1916.026662413055 ;


        !constraint, expr=table(survey,L4T.MBH.0250,X)=  - 1.9965056413325892   *table(survey,L4T.MBH.0250,Z) + 5742.904943514149   ; !Line2 in Mathematica
        !constraint, expr=table(survey,L4T.MBH.0450,X)= -17.492866081265376    *table(survey,L4T.MBH.0450,Z) + 35146.407324809916  ; !Line3 in Mathematica
        !constraint, expr=table(survey,L4T.MBH.0650,X)=   2.7383267026245885   *table(survey,L4T.MBH.0650,Z) - 3236.448213506853   ; !Line4 in Mathematica

        !constraint, expr=table(survey,L4T.MBV.1250,X)=   2.7383267026353466   *table(survey,L4T.MBV.1250,Z) - 3236.44821352739   ; !Line5 in Mathematica
        !constraint, expr=table(survey,L4T.MBV.1250,X)/sqrt(table(survey,L4T.MBV.1250,Z)^2+table(survey,L4T.MBV.1250,Y)^2)= 0.258122;
         
        !constraint, expr=table(survey,L4T.MBV.1550,X)=   2.738326702623939    *table(survey,L4T.MBV.1550,Z) - 3236.4482135055728  ; !Line6 in Mathematica
        
        simplex, calls = 1000, tolerance = 1.0E-25;
!       lmdif,   calls = 50, tolerance = 1.0E-25;
 endmatch;*/


/*******************************************************************************
 * twiss
 *******************************************************************************/

 maketwiss: macro=
 {
     ptc_create_universe;
     ptc_create_layout,model=2,method=6,nst=5,exact,time=false;
     ptc_twiss,table=ptc_twiss,BETA0=INITBETA0,icase=5,no=1; ! ,file="twiss";
     ptc_end;
 };


/*******************************************************************************
 * Use
 *******************************************************************************/
 use, sequence=L4T;


/***************************************************
 * Write ptc_twiss table. NB! Values at end of elements
 ***************************************************/
set,  format="-18s";
set,  format="10.5f";

! ns    ~ longitudinal number of sigma
! nt    ~ transverse   number of sigma
! dimxc ~ size-x with dispersion, dimyc ~ size-y with dispersion,
! value, beam->ex, beam->ey, beam->sige;

 ns = 2;
 nt = 3;
 dimxc := sqrt( nt^2*table(ptc_twiss,beta11)*beam->ex + ns^2*(beam->sige*table(ptc_twiss,disp1))^2 );
 dimyc := sqrt( nt^2*table(ptc_twiss,beta22)*beam->ey + ns^2*(beam->sige*table(ptc_twiss,disp3))^2 );



 exec, maketwiss; ! Needs to be executed first, to make beta11, disp1, etc.
 select,flag=ptc_twiss,clear;
 select,flag=ptc_twiss, column = name,s,l,alfa11,beta11,disp1,dimxc,alfa22,beta22,disp3,dimyc;
 exec, maketwiss;
 write,table=ptc_twiss,file="../out/l4t.out";

 select,flag=ptc_twiss,clear;
 select,flag=ptc_twiss, column = keyword,name,parent,l,s;
 exec, maketwiss;
 write,table=ptc_twiss,file="../out/l4t_config.out";

 select,flag=ptc_twiss,clear;
 select,flag=ptc_twiss, column = name,angle,k1L,k2L,k3L,beta11,beta22,disp1,disp3,x,y,alfa11,alfa22,mu1,mu2,disp2,disp4,px,py;
 exec, maketwiss;
 write,table=ptc_twiss,file="../out/l4t_optics.out";

 select,flag=ptc_twiss,clear;
 select,flag=ptc_twiss, column = name,s,beta11,beta12,beta21,beta22,alfa11,alfa12,alfa21,alfa22;
 exec, maketwiss;
 write,table=ptc_twiss,file="../out/l4t_offdiag.out";




/*******************************************************************************
 * Plot l4t
 *******************************************************************************/
option, -info;
option, -echo;

resplot;
setplot, post=2;

plot, title='l4t'   , table=ptc_twiss
                   , haxis=s
                   , vaxis1=beta11,beta22
                   , style:=100,symbol:=4,colour=100
                   , file = "../out/l4t";

plot, title='l4t'   , table=ptc_twiss
                   , haxis=s
                   , vaxis1=beta11,beta22
                   , vaxis2=disp1
                   , range=#S/#e
                   , style:=100, symbol:=4, colour=100;

plot, title='l4t'   , table=ptc_twiss
                   , haxis=s
                   , vaxis1=disp1,disp3
                   , range=#S/#e
                   , style:=100, symbol:=4, colour=100;

/* Use >gv madx01.eps  to plot */


/*******************************************************************************
 * Clean up (remove Maxwellian_bend_for_ptc.txt )
 *******************************************************************************/

system, "rm Maxwellian_bend_for_ptc.txt";


stop;





