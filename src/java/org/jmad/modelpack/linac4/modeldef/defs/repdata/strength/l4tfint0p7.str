!from Alessandra Lombardi  https://edms.cern.ch/document/1391781/0.1
!K1 = Gradient/Brho
!Brho is calculated at 160 MeV
!Brho =0.57112(GeV/c)*3.3356=1.905027872 Tm

 const brho_l4 = 1.905027872;

/************************************************************************************************************/
/*                       STRENGTHS                                                                          */
/************************************************************************************************************/

 k1.L4T.MQD.0110 =  7.51510/brho_l4;
 k1.L4T.MQF.0210 = -7.82351/brho_l4;
 k1.L4T.MQD.0310 =  2.72079/brho_l4;
 k1.L4T.MQF.0410 = -4.76669/brho_l4;
 k1.L4T.MQF.0510 = -4.00000/brho_l4;
 k1.L4T.MQD.0610 =  2.33330/brho_l4;
 k1.L4T.MQF.0710 = -1.92969/brho_l4;
 k1.L4T.MQD.0810 =  0.92647/brho_l4;
 k1.L4T.MQF.0910 = -0.85037/brho_l4;
 k1.L4T.MQD.1010 =  0.35200/brho_l4;
 k1.L4T.MQD.1110 =  2.33221/brho_l4;
 k1.L4T.MQF.1210 = -2.34466/brho_l4;
 k1.L4T.MQD.1310 =  5.25000/brho_l4;
 k1.L4T.MQF.1410 = -3.38333/brho_l4;
 k1.L4T.MQD.1510 =  5.25000/brho_l4;
 k1.L4T.MQD.1610 =  4.47953/brho_l4;
 k1.L4T.MQF.1710 = -2.91959/brho_l4;


! The signs of all K1 from Alessandra's input are inverted, so that MADX can simulate a H- beam (negative charge).

 L4T.ACDB.1075        , RM21 = 0.01100, RM42 = 0.01100;
 L4T.MQD.0110         , K1 = -k1.L4T.MQD.0110;
 L4T.MQF.0210         , K1 = -k1.L4T.MQF.0210;
 L4T.MQD.0310         , K1 = -k1.L4T.MQD.0310;
 L4T.MQF.0410         , K1 = -k1.L4T.MQF.0410;
 L4T.MQF.0510         , K1 = -k1.L4T.MQF.0510;
 L4T.MQD.0610         , K1 = -k1.L4T.MQD.0610;
 L4T.MQF.0710         , K1 = -k1.L4T.MQF.0710;
 L4T.MQD.0810         , K1 = -k1.L4T.MQD.0810;
 L4T.MQF.0910         , K1 = -k1.L4T.MQF.0910;
 L4T.MQD.1010         , K1 = -k1.L4T.MQD.1010;
 L4T.MQD.1110         , K1 = -k1.L4T.MQD.1110;
 L4T.MQF.1210         , K1 = -k1.L4T.MQF.1210;
 L4T.MQD.1310         , K1 = -k1.L4T.MQD.1310;
 L4T.MQF.1410         , K1 = -k1.L4T.MQF.1410;
 L4T.MQD.1510         , K1 = -k1.L4T.MQD.1510;
 L4T.MQD.1610         , K1 = -k1.L4T.MQD.1610;
 L4T.MQF.1710         , K1 = -k1.L4T.MQF.1710;

 L4T.MBH.0250         , ANGLE :=  0.407243448374, E1 := 0.407243448374/2, E2 := 0.407243448374/2, HGAP=0.025, FINT=0.7; !angle from survey
 L4T.MBH.0450         , ANGLE :=  0.407243448374, E1 := 0.407243448374/2, E2 := 0.407243448374/2, HGAP=0.025, FINT=0.7; !angle from survey
 L4T.MBH.0650         , ANGLE :=  0.407243448374, E1 := 0.407243448374/2, E2 := 0.407243448374/2, HGAP=0.025, FINT=0.7; !angle from survey
 L4T.MBV.1250         , ANGLE :=  0.2526079601,   E1 := 0.2526079601/2,   E2 := 0.2526079601/2,   HGAP=0.025, TILT = -1.5707963268; !angle from survey
 L4T.MBV.1550         , ANGLE := -0.2526079601,   E1 :=-0.2526079601/2,   E2 :=-0.2526079601/2,   HGAP=0.025, TILT = -1.5707963268; !angle from survey
! LT.BHZ20             , ANGLE := -0.420476422710364277 + 8.90625e-14 , E1:=-0.420476422710364277/2,E2:=-0.420476422710364277/2, HGAP=0.050;


return;